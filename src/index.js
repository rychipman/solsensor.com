import React from 'react';
import { render } from 'react-dom';
import DevTools from 'mobx-react-devtools';
import { Provider } from 'mobx-react';
import registerServiceWorker from './registerServiceWorker';

import 'semantic-ui-css/semantic.min.css';

import {
	BrowserRouter as Router,
	Route,
} from "react-router-dom";

import Root from './components/Root';
import Login from './components/Login';
import Register from './components/Register';
import Sensors from './components/Sensors';
import Layout from './components/Layout';
import Notifications from './components/Notifications';
import authStore from './stores/AuthStore';
import commonStore from './stores/CommonStore';
import sensorStore from './stores/SensorStore';
import notificationStore from './stores/NotificationStore';

const stores = {
	authStore,
	commonStore,
	sensorStore,
	notificationStore,
};

render(
	<Provider {...stores}>
	  <Router>
		<div>
			<Layout>
				<DevTools />
				<Route exact path="/" component={Root} />
				<Route exact path="/login" component={Login} />
				<Route exact path="/register" component={Register} />
				<Route exact path="/sensors" component={Sensors} />
			</Layout>
			<Notifications />
		</div>
	  </Router>
	</Provider>,
  document.getElementById("root")
);
registerServiceWorker();
