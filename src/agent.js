import axios from "axios";

var baseURL;
if (process.env.NODE_ENV === 'development') {
	baseURL = 'http://localhost:8080'
} else {
	baseURL = 'https://api.solsensor.com'
}

const agent = axios.create({
	baseURL,
	timeout: 1000,
})

const api = {
	login: (email, password) =>
		agent.request({
			method: 'post',
			url: '/user/login',
			data: { email, password },
		}),
	register: (email, password) =>
		agent.request({
			method: 'post',
			url: '/user/register',
			data: { email, password },
		}),
	getData: (token) =>
		agent.request({
			method: 'get',
			url: '/data',
			headers: { 'X-Api-Token': token },
		}),
	getSensors: (token) =>
		agent.request({
			method: 'get',
			url: '/sensors',
			headers: { 'X-Api-Token': token },
		}),
};

export default api;
