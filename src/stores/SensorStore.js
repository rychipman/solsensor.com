import { action, observable } from "mobx";
import agent from "../agent";
import commonStore from "./CommonStore";
import notificationStore from "./NotificationStore";

class SensorStore {
	id
	@observable readings = undefined

	@action pullData() {
		console.log("pullData request started")
		agent.getData(commonStore.token).then(this.pullDataResult)
	}

	@action.bound
	pullDataResult(res) {
		const { success, error = '', data } = res.data;
		if (!success) {
			console.log("pullData request failed")
			notificationStore.error('failed to fetch sensor readings', error);
			return
		}
		this.readings = data.readings.map((n, idx) => (
			{time: idx, power: n, voltage: n+1}
		))
		console.log("pullData request succeeded")
	}
}

class SensorsStore {
	@observable sensors = undefined

	@action pullData() {
		agent.getSensors(commonStore.token).then(this.pullDataResult)
	}

	@action.bound
	pullDataResult(res) {
		const { success, error = '', data } = res.data;
		if (!success) {
			console.log("pullData request failed")
			notificationStore.error('failed to fetch sensor readings', error);
			return
		}
		this.sensors = data.sensors.map(s => new SensorStore())
		console.log("pullSensors request succeeded")
		this.sensors.map(s => s.pullData());
	}
}

export default new SensorsStore();
