import { action, observable } from "mobx";

class CommonStore {
	@observable token = undefined;
	@observable currentUser = undefined;
	@observable appLoaded = false;

	@action setUser(user) {
		this.currentUser = user;
	}

	@action setToken(token) {
		this.token = token;
	}

	@action setAppLoaded() {
		this.appLoaded = true;
	}
}

export default new CommonStore();
