import { action, observable } from "mobx";
import commonStore from "./CommonStore";
import agent from '../agent';

class AuthStore {
	@observable inProgress = false;
	@observable message = {};

	@observable values = {
		email: '',
		password: '',
	};

	@action setEmail(email) {
		this.values.email = email;
	}

	@action setPassword(password) {
		this.values.password = password;
	}

	@action login() {
		this.inProgress = true;
		agent.login(this.values.email, this.values.password)
			.then(this.loginResult);
	}

	@action.bound
	loginResult(res) {
		const { success, data = {}, error = '' } = res.data;
		const { email, token } = data;
		this.inProgress = false;
		this.values.password = '';
		if (success) {
			this.message = {
				type: 'success',
				header: 'Login Successful',
				content: 'You may now use your account',
			};
			commonStore.setUser(email);
			commonStore.setToken(token);
		} else {
			this.message = {
				type: 'failure',
				header: 'Login Failed',
				content: error,
			};
		}
	}

	@action register() {
		this.inProgress = true;
		agent.register(this.values.email, this.values.password)
			.then(this.registrationResult);
	}

	@action.bound
	registrationResult(res) {
		const { success, error = '' } = res.data;
		this.inProgress = false;
		this.values.password = '';
		if (success) {
			this.message = {
				type: 'success',
				header: 'Registration Successful',
				content: 'You may now login with the newly-registered account',
			};
		} else {
			this.message = {
				type: 'failure',
				header: 'Registration Failed',
				content: error,
			};
		}
	}

	@action logout() {
		commonStore.setUser(undefined);
		commonStore.setToken(undefined);
	}

	@action reset() {
		this.inProgress = false;
		this.message = {};
		this.values = {
			email: '',
			password: '',
		};
	}
}

export default new AuthStore();
