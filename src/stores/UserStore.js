import { action, observable } from "mobx";

class UserStore {
	@observable inProgress = false;
	@observable errors = undefined;

	@observable values = {
		email: '',
		password: '',
	};

	@action setEmail(email) {
		this.values.email = email;
	}

	@action setPassword(password) {
		this.values.password = password;
	}

	@action reset() {
		this.values.email = '';
		this.values.password = '';
	}

	@action login() {
		this.inProgress = true;
		console.log("logging in...");
		setTimeout(() => {
			console.log("logged in!");
			this.inProgress = false;
		}, 2000);
	}

	@action register() {
		// TODO
	}

	@action logout() {
		// TODO
	}
}

export default AuthStore;
