import React from "react";
import { toJS } from "mobx";
import { inject, observer } from "mobx-react";
import {
  Container,
  Loader,
  Segment,
} from "semantic-ui-react";
import Chart from './Chart';

@inject("sensorStore")
@observer
class SensorsPage extends React.Component {
  render() {
    const { sensorStore } = this.props;
    return (
	  <Container width='50%'>
	  <h2>Sensors</h2>
	  <button onClick={() => sensorStore.pullData()}>Get Data</button>
	  {sensorStore.sensors
	   ? sensorStore.sensors.map((s, idx) => {
			const readings = toJS(s.readings);
			const loading = !readings;
			return (
				<Segment key={idx}>
					<button onClick={() => s.pullData()}>Get Data</button>
					<Loader active={loading} />
					<Chart data={readings} />
				</Segment>
			)
		 })
	   : null
	  }
	  </Container>
	)
  }
}


export default SensorsPage;