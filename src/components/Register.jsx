import React from "react";
import { inject, observer } from "mobx-react";
import {
    Container,
    Form,
	Message,
	Segment,
} from "semantic-ui-react";

@inject("authStore")
@observer
class RegisterPage extends React.Component {

render() {
	const { authStore } = this.props;
	const { values, message, inProgress } = authStore;
    return (
      <Container style={{ width: '40%' }}>
        <Message
		  success={message.type === 'success'}
		  error={message.type === 'failure'}
		>
            <Message.Header content={message.header || 'Register'} />
            <Message.Content content={message.content || 'Create your SolSensor account'} />
        </Message>
		<Segment>
		<Form loading={inProgress} onSubmit={() => authStore.register()}>
			<Form.Input
			  label='Email'
			  placeholder='email'
			  value={values.email}
			  onChange={this.handleEmailChange}
			/>
			<Form.Input
			  type='password'
			  label='Password'
			  placeholder='password'
			  value={values.password}
			  onChange={this.handlePasswordChange}
			/>
			<Form.Button content='Register' />
		</Form>
		</Segment>
      </Container>
    );
  }

	handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);
	handleEmailChange = e => this.props.authStore.setEmail(e.target.value);
}

export default RegisterPage;