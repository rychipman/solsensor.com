import React from "react";
import { inject, observer } from "mobx-react";
import {
    Container,
    Form,
	Message,
	Segment,
} from "semantic-ui-react";

@inject("authStore")
@observer
class LoginPage extends React.Component {

render() {
	const { authStore } = this.props;
	const { values, message, inProgress } = authStore;
    return (
      <Container style={{ width: '40%' }}>
        <Message
		  success={message.type === 'success'}
		  error={message.type === 'failure'}
		>
            <Message.Header content={message.header || 'Login'} />
            <Message.Content content={message.content || 'Sign in to your SolSensor account'} />
        </Message>
		<Segment>
		<Form loading={inProgress} onSubmit={() => authStore.login()}>
			<Form.Input
			  label='Email'
			  placeholder='email'
			  value={values.email}
			  onChange={this.handleEmailChange}
			/>
			<Form.Input
			  type='password'
			  label='Password'
			  placeholder='password'
			  value={values.password}
			  onChange={this.handlePasswordChange}
			/>
			<Form.Button content='Login' />
		</Form>
		</Segment>
      </Container>
    );
  }

	handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);
	handleEmailChange = e => this.props.authStore.setEmail(e.target.value);
}

export default LoginPage;