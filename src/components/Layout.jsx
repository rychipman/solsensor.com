import React from "react";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import {
    Container,
	Dropdown,
	Image,
	Menu,
} from 'semantic-ui-react';
import logo from '../sun.svg';

const menuContents = ({ commonStore, authStore }) => {
    const { currentUser } = commonStore;
    if (!currentUser || currentUser === '') {
        return ([
            <Menu.Item as={Link} to='/login' key='login'>
                Log In
            </Menu.Item>,
            <Menu.Item as={Link} to='/register' key='register'>
                Register
            </Menu.Item>,
        ])
    }
    return ([
        <Dropdown item text={currentUser} position='right' key='dropdown'>
            <Dropdown.Menu>
                <Dropdown.Item
                  text='Profile'
                  as={Link}
                  to={'/user/'+currentUser}
                />
                <Dropdown.Item
                  text='Log Out'
                  onClick={() => authStore.logout()}
                />
            </Dropdown.Menu>
        </Dropdown>,
        <Menu.Item as={Link} to='/sensors' key='sets'>Sensors</Menu.Item>,
    ])
}
const MenuContents = inject('commonStore', 'authStore')(observer(menuContents));

const Layout = ({ children }) => (
    <div>
        <Menu fixed='top' borderless>
            <Container>
            <Menu.Item header as={Link} to='/'>
                <Image
                  size='mini'
                  src={logo}
                  style={{ marginRight: '1.5em' }}
                />
                Sol Sensor
            </Menu.Item>
            <MenuContents />
            </Container>
        </Menu>
        <Container style={{ marginTop: '7em' }}>
            {children}
        </Container>
    </div>
)

export default Layout