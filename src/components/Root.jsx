import React from "react";

const RootPage = () => (
  <div>
    <p>This is the root page</p>
    <p>There is nothing interesting here</p>
  </div>
);

export default RootPage;