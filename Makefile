.PHONY: build rsync deploy

build:
	yarn build

rsync:
	rsync -az --force --delete --progress -e ssh ./build/ nu:/var/www/solsensor.com/html

deploy: build rsync
